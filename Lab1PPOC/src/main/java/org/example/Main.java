package org.example;

import org.example.Operation.Add;
import org.example.Operation.Divide;
import org.example.Operation.Multiply;
import org.example.Operation.Subtract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));

        Integer first = 0, second = 0;
        String op;

            System.out.println("Write the first number");
            first= Integer.valueOf(reader.readLine());
            System.out.println("Write the second number");
            second= Integer.valueOf(reader.readLine());
            System.out.println("Write the operation");
            op= reader.readLine();
            if(Objects.equals(op, "+")){
                System.out.println(new Add(first,second).exec());
            }
            if(Objects.equals(op, "-")){
                System.out.println(new Subtract(first,second).exec());
            }
            if(Objects.equals(op, "/")){
                System.out.println(new Divide(first,second).exec());
            }
            if(Objects.equals(op, "*")){
                System.out.println(new Multiply(first,second).exec());

        }
    }
}