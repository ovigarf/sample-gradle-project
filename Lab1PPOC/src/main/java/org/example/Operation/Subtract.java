package org.example.Operation;

public class Subtract {
    Integer number1;
    Integer number2;

    public Subtract(Integer number1, Integer number2){
        this.number1=number1;
        this.number2=number2;
    }
    public Integer exec(){
        return number1-number2;
    }
}
