package org.example.Operation;

public class Add implements Operation{
    Integer number1;
    Integer number2;

    public Add(Integer number1, Integer number2){
        this.number1=number1;
        this.number2=number2;
    }
    public Integer exec(){
        return number1+number2;
    }
}
