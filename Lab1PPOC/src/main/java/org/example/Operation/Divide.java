package org.example.Operation;

public class Divide {
    Integer number1;
    Integer number2;

    public Divide(Integer number1, Integer number2){
        this.number1=number1;
        this.number2=number2;
    }
    public Integer exec() throws Exception {
        if(number2==0)
            throw new Exception("Div by 0");
        return number1/number2;
    }
}
