package com.example;

import org.example.Operation.Add;
import org.example.Operation.Divide;
import org.example.Operation.Multiply;
import org.example.Operation.Subtract;
import org.junit.jupiter.api.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestOperations {

    @Test
    public void addShouldAdd() {
        assertThat(new Add(3,4).exec(), is(7));
        assertThat(new Add(20,7).exec(), is(27));
        assertThat(new Add(-1,4).exec(), is(3));
        assertThat(new Add(3,-3).exec(), is(0));
        assertThat(new Add(55,55).exec(), is(110));
        assertThat(new Add(1,1).exec(), is(2));

    }
    @Test
    public void subtractShouldSubtract() {
        assertThat(new Subtract(3,4).exec(), is(-1));
        assertThat(new Subtract(20,7).exec(), is(13));
        assertThat(new Subtract(-1,4).exec(), is(-5));
        assertThat(new Subtract(3,-3).exec(), is(6));
        assertThat(new Subtract(55,55).exec(), is(0));
        assertThat(new Subtract(1,1).exec(), is(0));

    }
    @Test
    public void divideShouldDivide() throws Exception {
        assertThat(new Divide(4,4).exec(), is(1));
        assertThat(new Divide(20,2).exec(), is(10));
        assertThat(new Divide(1,1).exec(), is(1));

        Exception exception = assertThrows(Exception.class, () -> {
            new Divide(3,0).exec();
        });
        assertTrue(exception.getMessage().contains("Div by 0"));
        assertThat(new Divide(55,55).exec(), is(1));
        assertThat(new Divide(1,1).exec(), is(1));

    }
    @Test
    public void multiplyShouldMultiply() {
        assertThat(new Multiply(3,4).exec(), is(12));
        assertThat(new Multiply(20,7).exec(), is(140));
        assertThat(new Multiply(-1,4).exec(), is(-4));
        assertThat(new Multiply(3,-3).exec(), is(-9));
        assertThat(new Multiply(55,2).exec(), is(110));
        assertThat(new Multiply(1,1).exec(), is(1));

    }

}
